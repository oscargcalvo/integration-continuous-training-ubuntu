#!groovy
import groovy.json.JsonSlurperClassic
import org.apache.commons.lang.StringEscapeUtils
/***********************************************************************************************************************
 * Documentation
 ***********************************************************************************************************************
 *
 * Deployment and branch strategy overview
 * =======================================
 *
 * There are 4 branches within this deployment strategy in use within this pipeline script:
 *
 * - feature branch
 * - master branch
 * - develop branch
 * - uat branch
 * - hotfix branch
 *
 * All new features should be developed on a feature branch with the naming convention "feature/ft_(US-* or TKT-*)_my_new_feature".
 * This will then allow Jenkins to merge the feature branch into develop (check only) and build it against our buildchk org
 * and run some quality checks. The develop branch contains all of the latest updates which are ready for deployment to
 * test & uat. After a successful build to UAT our Buildchk Sandbox is updated with the same code.
 *
 * After a merge into the master branch our deployment to Production kicks in. Upon successful deployment to Production
 * our Validation Sandbox is also updated to mirror production again.
 *
 * - TEST
 * - QA
 * - UAT
 * - PRODUCTION
 * - BUILDCHK (Will be used to test the Pull Request before merging in to develop branch)
 * - VALIDATE (Will be used to periodically build the dev branch to empty production state sandbox)
 *
 * Hotfix branches will deploy the branch to a sandbox called HOTFIXTEST and then will deploy the branch to PRODUCTION.
 * Due to the nature of the updates it is a manual activity to backport the change back into master, however this 
 * pipeline will create automatically a pull request to merge the branch back into the master branch upon successful
 * deployment to PRODUCTION.
 *
 * Linux and Windows based Jenkins installations
 * =============================================
 *
 * The Jenkinsfile has set up to work with both Linux and Windows Jenkins installations. If you are adjusting this
 * file be aware if you migrate to a different operating system you may have to adjust the script accordingly.
 *
 * Plugins required
 * ================
 * All of the following plugins must be installed for this Jenkinsfile to operate correctly:
 *
 * - BitBucket Branch Source Plugin
 * - BitBucket Build Status Notifier Plugin
 * - Blue Ocean
 * - Build Timeout Plugin
 * - Custom Tools Plugin
 * - Email Extension Plugin
 * - HTML Publisher Plugin
 * - Lockable Resources Plugin
 * - PMD Plug-in
 * - Static Analysis Collector Plugin
 *
 * Connected Apps
 * ==============
 *
 * All sandboxes and Production must have a connected app to be created in order for the SFDX tooling to be able to
 * login and deploy the metadata. The connected apps must be set up to use OAuth 2.0 JWT flow. The following permissions
 * are required for the app:
 *
 * - api
 * - refresh_token, offline_access
 * - web
 *
 * Additionally, you must set the callback URL to "http://localhost:1717/OauthRedirect" and enable digital signatures
 * and upload a self-signed certificate. You can create the self-sign certificates using the following commands:
 *
 * openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
 * openssl rsa -passin pass:x -in server.pass.key -out server.key
 * openssl req -new -key server.key -out server.csr
 * openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt
 * cat server.crt
 * cat server.key
 *
 * The server.crt is the certificate you will need to upload to the connected app per org.
 *
 * IMPORTANT: You must set each connected as admin approved for the integration users profile. Not doing this will
 * cause issues when attempting to login using the JWT flow.
 *
 * Jenkins configuration
 * =====================
 *
 * 1. Downloaded all plugins
 * 2. Created BitBucket credentials
 * 3. Create new Multi-Pipeline job
 * 4. Create OAuth app in BitBucket
 * 5. Create BitBucketOuath credential with username client ID and password the secret
 * 6. Goto Configure System and configure the BitBucket Pull Request Status Notifier to use OAuth credentials
 * 7. Install Salesforce DX CLI
 * 8. Configure Custom Tools (Jenkins/Global Tool Configuration) > Name as 'toolbelt' > c:\sfdx / /var/location/installed/sfdx
 *    (point only to the directory) and set the installation directory. Do not use install automatically.
 * 9. Configure Ant (Jenkins/Global Tool Configuration) > Name as 'Ant' & install automatically one of the latest versions (If not installed directly on the server)
 * 10. Manually run "sfdx update" - not doing this on Windows will lead to an error regarding filename too long
 * 11. Create a new credential of type "Secret file" and upload the certificates per hub and each sandbox.
 * 	  (Update the variables in this script accordingly).
 * 12. Keep running the job and approving script requests
 *
 * Performance warning: if you are using a brand new instance of Jenkins then it will most likely will have 256mb
 * reserved, you may yeild improved performance if you increase the memory to 1024mb.
 *
 * IMPORTANT: Do not rename pipeline jobs within Jenkins as this can lead to extremely slow performance. If you need
 * to rename the job, recreate it from scratch.
 *
 * Windows specific issues
 * =======================
 *
 * You will need to ensure that Jenkins is set to run as a user in Computer Management > services.
 * Ensure that the PATH environmental variables includes a path to SFDX. The toolkit has an issue where
 * if you call the program using a full path it fails to execute.
 *
 * Linux specific issues
 * =======================
 * While using "Amazon Linux 2 LTS Candidate AMI 2017.12.0 (HVM)" you can bump into the below issue.
 * 
 * ERROR:  Command failed with response. - 
 * (secret-tool:10535): GLib-GObject-CRITICAL **: g_object_unref: assertion 'G_IS_OBJECT (object)' failed secret-tool: Cannot autolaunch D-Bus without X11 $DISPLAY
 *
 * To solve this:
 * Go to /usr/bin/ &
 * delete the secret-tool file with command: sudo rm secret-tool
 * 
 * First run situation
 * ===================
 *
 * Due to the security sandboxing functionality introduced in Jenkins you will have to run the build multiple times
 * and approve method calls in the manage jenkins script approvals sections. This is UNAVOIDABLE unless you whitelist
 * the complete Jenkinsfile and run it not in a sandbox. Some platforms such as Cloudbees you will not be able to 
 * disable the sandbox and must approve each and every restricted code request. 
 *
 * !Important! Tag your very first commit on your git repository with "production/0" else the deltaDeploy will not pickup any tags & fail.
 *
 * @author 		Adam Marchbanks (amarchbanks@deloitte.nl)
 * @created 	August 2017
 * @copyright 	Deloitte NL. All Rights Reserved.
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Stage definitions
 **********************************************************************************************************************/


node {

	// Job properties																												
	properties([
			// Discard old logs to ensure we don't consume too much disk space
			// Set numToKeepStr to a higher value if you have sufficient disk space
			// available
			[
					$class  : 'jenkins.model.BuildDiscarderProperty',
					strategy: [
							$class: 'LogRotator',
							numToKeepStr: '10'
					]
			]
	])

	// Constants
	def BUILD_NUMBER = env.BUILD_NUMBER
	def SCRATCH_ORG_SFDC_USERNAME
	def SCM_URL
	def SANDBOX_HOST = 'https://test.salesforce.com'
	def PROD_HOST = 'https://login.salesforce.com'
	def UNIT_TESTS_OUTPUT_DIR = 'reports'
	def CHANGE_LOG
	def CHANGED_FILES
	int NUMBER_NEXT_TAG = 0

	// Set to true if this project is Salesforce DX source code defined. E.g. it is not using the older traditional
	// metadata layout. When this is set to false the script expects your source code to be in the mdapi/ folder.
	def DX_FORMAT_PROJECT = false

	// If not using scratch orgs, do you wish to build against a build check sandbox. Check only deployment.
	def PR_BUILD_CHECK_SANDBOX = true

	// Set the following to true if you wish to update Tracker with the change logs
	def TRACKER_CHANGELOG_NOTIFIER_ENABLED = false

	// Enable BitBucket Pull Request Notifier
	def PR_ENABLE_NOTIFIER = true

	// GIT Provider Credentials - this must be a username/password credential due to at the time of writing no plugin
	// being available for GIT Publisher within Jenkins Pipelines
	def GIT_CRED_ID = 'BitBucket'

	// HUB
	def HUB_USERNAME = ""
	def HUB_HOST = PROD_HOST
	def HUB_JWT_KEY_CRED = ""
	def HUB_CONNECTED_APP_CONSUMER_KEY = ""

	// TEST
	def TEST_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def TEST_CONNECTED_APP_CONSUMER_KEY = "3MVG9lcxCTdG2VbtWAnBLcHTY.4HLXuySvdmwMkcUIgcxBcmD21oBVxrbvAsAmPQ320WPztLwzerDWQbrkl_b"
	def TEST_HOST = SANDBOX_HOST;
	def TEST_USERNAME = 'admin@deloitte.es.sgre.sitcheck47'
	def TEST_ORGNAME = 'sit'

	// QA
	def QA_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def QA_CONNECTED_APP_CONSUMER_KEY = "3MVG9LzKxa43zqdKmeTTNjkqgD_oLVDbvoqU1m2VSs.KYXc1o37d7Nk6H3koQ6_D9n8tKeuY5h73QboopKPTR"
	def QA_HOST = SANDBOX_HOST;
	def QA_USERNAME = 'admin@deloitte.es.sgre.qacheck47'
	def QA_ORGNAME = 'qa'

	// UAT
	def UAT_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def UAT_CONNECTED_APP_CONSUMER_KEY = "3MVG9ZPHiJTk7yFyo2kgZvLTvpmN.15SoCyL1JDAjIk0JcihV8PxN.xKvIKjndjgGkQOqFXALmq0VuM5ZY4Pw"
	def UAT_HOST = SANDBOX_HOST;
	def UAT_USERNAME = 'admin@deloitte.es.sgre.uatcheck47'
	def UAT_ORGNAME = 'uat'

	// PRODUCTION
	def PRODUCTION_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def PRODUCTION_CONNECTED_APP_CONSUMER_KEY = "3MVG9LzKxa43zqdKmeTTNjkqgD50D_LIH9vcJhoD5lCJzgjmVT2nWNY8k5YycE5ZdgtCrwfbMau_CFnjDQSfZ"
	def PRODUCTION_HOST = SANDBOX_HOST;
	def PRODUCTION_USERNAME = 'admin@deloitte.es.sgre.validate47'
	def PRODUCTION_ORGNAME = 'production'

	// Pull request sandbox credentials, specify if not using scratch orgs
	def PR_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def PR_CONNECTED_APP_CONSUMER_KEY = "3MVG9lcxCTdG2VbtWAnBLcHTY.4HLXuySvdmwMkcUIgcxBcmD21oBVxrbvAsAmPQ320WPztLwzerDWQbrkl_b"
	def PR_HOST = SANDBOX_HOST;
	def PR_USERNAME = 'admin@deloitte.es.sgre.sitcheck47'
	def PR_ORGNAME = 'BuildCheck'

	// Validate sandbox credentials
	def VAL_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def VAL_CONNECTED_APP_CONSUMER_KEY = "3MVG9LzKxa43zqdKmeTTNjkqgD50D_LIH9vcJhoD5lCJzgjmVT2nWNY8k5YycE5ZdgtCrwfbMau_CFnjDQSfZ"
	def VAL_HOST = SANDBOX_HOST;
	def VAL_USERNAME = 'admin@deloitte.es.sgre.validate47'
	def VAL_ORGNAME = 'validate'

    // QACheck sandbox credentials
	def QACHECK_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def QACHECK_CONNECTED_APP_CONSUMER_KEY = "3MVG9LzKxa43zqdKmeTTNjkqgD_oLVDbvoqU1m2VSs.KYXc1o37d7Nk6H3koQ6_D9n8tKeuY5h73QboopKPTR"
	def QACHECK_HOST = SANDBOX_HOST;
	def QACHECK_USERNAME = 'admin@deloitte.es.sgre.qacheck47'
	def QACHECK_ORGNAME = 'QACheck'

    // UATCheck sandbox credentials
	def UATCHECK_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def UATCHECK_CONNECTED_APP_CONSUMER_KEY = "3MVG9ZPHiJTk7yFyo2kgZvLTvpmN.15SoCyL1JDAjIk0JcihV8PxN.xKvIKjndjgGkQOqFXALmq0VuM5ZY4Pw"
	def UATCHECK_HOST = SANDBOX_HOST;
	def UATCHECK_USERNAME = 'admin@deloitte.es.sgre.uatcheck47'
	def UATCHECK_ORGNAME = 'UATCheck'

	// Tracker
	def TRACKER_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def TRACKER_CONNECTED_APP_CONSUMER_KEY = ""
	def TRACKER_HOST = PROD_HOST;
	def TRACKER_USERNAME = ''
	def TRACKER_ORGNAME = 'tracker'


	// Define the custom tool which points towards the SFDX installation
	// This is here for Linux systems only, Windows must have the SFDX executeable within PATH defined
	def toolbelt = tool 'toolbelt'
	def sfdx_path = tool 'sfdx_path'

	try {
		//C:\\Program Files\\Salesforce CLI\\bin
	withEnv(["PATH=${tool 'sfdx_path'};${env.PATH}"]) {
		echo "PATH : ${env.PATH}"
		// Clone the repository
		stage('Clone') {
			// Retrieve source
			checkout scm

			// Determine the SCRM url so we can push tags later on
			if(isUnix()){
				SCM_URL = sh(returnStdout: true, script: 'git config remote.origin.url').trim()
			} else {
				SCM_URL = bat(returnStdout: true, script: 'git config remote.origin.url').trim()
			}

			// Notify BitBucket pull request that we've started the build
			if(!isDevDeployable() && !!isUatDeployable() && !isHotfix()) {
				if(PR_ENABLE_NOTIFIER){
					try {
						bitbucketStatusNotify(
								buildState: 'INPROGRESS',
								buildKey: 'build',
								buildName: 'Build'
						)
					} catch(e){
						echo e.message
					}
				}
			}
		}


		// Determines all of the commits since the last production release. This is useful to know to understand what is
		// exactly included in this build when deploying.
		stage('Changes') {
			// Determine the changes
			passedBuilds = []
			lastSuccessfulBuild(passedBuilds, currentBuild);
			CHANGE_LOG = getChangeLog(passedBuilds)

			// Determine which files have been modified
			CHANGED_FILES = getChangedFiles(passedBuilds)

			// Retrieve the changed files using the git tags
			// This is needed as the CHANGED_FILES only
			def lastTag = ""
			def numberLastTag = ""
			
			if(isUnix()){
				if(isDevDeployable() || isPRForDevValidate()){
					lastTag = sh returnStdout: true, script: 'git describe --match "sit/*" --abbrev=0 HEAD 2> /dev/null'
				} else if(isQADeployable() || isPRForQAValidate()){
					lastTag = sh returnStdout: true, script: 'git describe --match "qa/*" --abbrev=0 HEAD 2> /dev/null'
				} else if(isUatDeployable() || isPRForUatValidate()){
					lastTag = sh returnStdout: true, script: 'git describe --match "uat/*" --abbrev=0 HEAD 2> /dev/null'
				} 
				else
				{
					lastTag = sh returnStdout: true, script: 'git describe --match "production/*" --abbrev=0 HEAD 2> /dev/null'
				}
				lastTag = lastTag.trim();
				echo "lastTag: ${lastTag}"
				//Get number last tag of branch in Bitbucket
				numberLastTag = lastTag.split('/')[1];
				echo "numberLastTag: ${numberLastTag}"
				NUMBER_NEXT_TAG = Integer.valueOf(numberLastTag).intValue() + 1;
				echo "NUMBER_NEXT_TAG: ${NUMBER_NEXT_TAG}"

				sh returnStdout: true, script: "git diff --name-only --relative --diff-filter AMR ${lastTag}...HEAD -- mdapi/src > mdapi/changes.txt"
			} else {
				if(isDevDeployable() || isPRForDevValidate()){
					lastTag = bat returnStdout: true, script: 'git describe --match "sit/*" --abbrev=0 HEAD 2> null'
				} else if(isQADeployable() || isPRForQAValidate()){
					lastTag = bat returnStdout: true, script: 'git describe --match "qa/*" --abbrev=0 HEAD 2> null'
				} else if(isUatDeployable() || isPRForUatValidate()){
					lastTag = bat returnStdout: true, script: 'git describe --match "uat/*" --abbrev=0 HEAD 2> null'
				} else {
					lastTag = bat returnStdout: true, script: 'git describe --match "production/*" --abbrev=0 HEAD 2> null'				
				}
				lastTag = lastTag.split('\n')[2].trim();
				echo "lastTag: ${lastTag}"
				//Get number last tag of branch in Bitbucket
				numberLastTag = lastTag.split('/')[1];
				echo "numberLastTag: ${numberLastTag}"
				NUMBER_NEXT_TAG = Integer.valueOf(numberLastTag).intValue() + 1;
				echo "NUMBER_NEXT_TAG: ${NUMBER_NEXT_TAG}"

				bat returnStdout: true, script: "git diff --name-only --relative --diff-filter AMR ${lastTag}...HEAD -- mdapi/src > mdapi/changes.txt"
			}

			// Stash the changes
			stash name: 'source', includes: 'mdapi/'
		}


		// Run delta deployments functionality
		if(!DX_FORMAT_PROJECT){
			stage('Delta Deployment') {
				// Ensure we retrieve the changes so we can run the delta deployments safely
				unstash 'source'

				// Run the delta deployments
				def command = 'java -jar build/modules/deltadeployments/deltadeployments.jar -C build/modules/deltadeployments/config.properties -F -L mdapi/changes.txt -P 47.0 -S mdapi/src -T mdapi/target'

				if(isUnix()){
					sh script: command
				} else {
					bat script: command
				}

				// Stash the build source code
				stash name: 'source', includes: 'mdapi/'
			}
		}


		// Run Apex PMD to determine health.
		//if(!isDevDeployable() && !isQADeployable() && !isUatDeployable() && !isProdDeployable() && !isHotfix()) {
			//stage('ApexPMD') {
				// Run ApexPMD
				//withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
					//if(isUnix()){
						//sh 'ant -f ./build/modules/pmd/build.xml -q'
					//} else {
						//bat 'ant -f ./build/modules/pmd/build.xml -q'
					//}
				//}

				// Publish the results
				//step([
						//$class: 'hudson.plugins.pmd.PmdPublisher',
						//checkstyle: 'build/modules/pmd/report/pmd.xml',
						//healthy: '700',	 					// Report health as 100% when the number of warnings is less than this value
						//unHealthy: '850',					// Report health as 0% when the number of warnings is greater than this value
						//unstableTotalAll: '850',			// annotation threshold
						//failedTotalAll: '1000',				// annotation threshold
						//usePreviousBuildAsReference: true,	// determines whether to always use the previous build as the reference build
				//])

				// Manually check the current build result as the error is being reported in another thread
				//if(currentBuild.result == 'FAILURE'){
					//error 'ApexPMD rule failure'
				//}
			//}
		//}


		// Run QA checks(for PullRequests)
		if(!isDevDeployable() && !isProdDeployable() && !isHotfix() && !isQADeployable() && !isUatDeployable()) {
			stage('QA Checks') {
				// If this a DX project then start up a scratch org, deploy the code, run the tests (and publish the
				// resultsand finally delete the scratch org.
				if(DX_FORMAT_PROJECT){

					withCredentials([file(credentialsId: HUB_JWT_KEY_CRED_ID, variable: 'jwt_key_file')]) {
						// Create a scratch org so we can deploy the Salesforce DX code to it
						// Authorise the hub org
						if(isUnix()){
							rc = sh returnStatus: true, script: "${toolbelt}/sfdx force:auth:jwt:grant --clientid ${HUB_CONNECTED_APP_CONSUMER_KEY} --username ${HUB_USERNAME} --jwtkeyfile ${jwt_key_file} --setdefaultdevhubusername --instanceurl ${HUB_HOST}"
						} else {
							rc = bat returnStatus: true, script: "sfdx force:auth:jwt:grant --clientid ${HUB_CONNECTED_APP_CONSUMER_KEY} --username ${HUB_USERNAME} --jwtkeyfile ${jwt_key_file} --setdefaultdevhubusername --instanceurl ${HUB_HOST}"
						}

						if (rc != 0) {
							error 'Hub org authorization failed'
						}

						// Need to pull out assigned username
						if(isUnix()){
							rmsg = sh returnStdout: true, script: "${toolbelt}/sfdx force:org:create --definitionfile config/project-scratch-def.json --json --setdefaultusername"
						} else {
							rmsg = bat returnStdout: true, script: "sfdx force:org:create --definitionfile config/project-scratch-def.json --json --setdefaultusername"
							rmsg = rmsg.split('\n')[2].trim()
						}

						def robj = jsonParse(rmsg)
						echo 'Response: ' + rmsg

						if (robj.status != 0){
							error 'Sratch org creation failed: ' + robj.message
						}

						SCRATCH_ORG_SFDC_USERNAME=robj.result.username
						robj = null

						try {
							// Push the Salesforce DX code to the scratch org so we can run the unit tests
							timeout(time: 30, unit: 'MINUTES') {
								if(isUnix()){
									rc = sh returnStatus: true, script: "${toolbelt}/sfdx force:source:push --targetusername ${SCRATCH_ORG_SFDC_USERNAME}"
								} else {
									rc = bat returnStatus: true, script: "sfdx force:source:push --targetusername ${SCRATCH_ORG_SFDC_USERNAME}"
								}

								if (rc != 0) {
									error 'Unable to deploy to scratch org'
								}
							}

							// Run the unit tests in the scratch org
							if(isUnix()){
								sh "mkdir -p ${UNIT_TESTS_OUTPUT_DIR}"
							} else {
								bat "if not exist \"${UNIT_TESTS_OUTPUT_DIR}\" mkdir \"${UNIT_TESTS_OUTPUT_DIR}\""
							}

							timeout(time: 120, unit: 'SECONDS') {
								if(isUnix()){
									sh returnStdout: true, script: "${toolbelt}/sfdx force:apex:test:run --testlevel RunLocalTests --outputdir ${UNIT_TESTS_OUTPUT_DIR} --resultformat junit --codecoverage --targetusername ${SCRATCH_ORG_SFDC_USERNAME} --json --loglevel debug || true"
								} else {
									bat returnStdout: true, script: "sfdx force:apex:test:run --testlevel RunLocalTests --outputdir ${UNIT_TESTS_OUTPUT_DIR} --resultformat junit --codecoverage --targetusername ${SCRATCH_ORG_SFDC_USERNAME} --json --loglevel debug"
								}
							}

							// Collect the results
							junit keepLongStdio: true, allowEmptyResults: true, testResults: "${UNIT_TESTS_OUTPUT_DIR}/**/*-junit.xml"

						} finally {
							// Shut down the scratch org, we don't need to maintain it any longer
							timeout(time: 120, unit: 'SECONDS') {
								if(isUnix()){
									rc = sh returnStatus: true, script: "${toolbelt}/sfdx force:org:delete --targetusername ${SCRATCH_ORG_SFDC_USERNAME} --noprompt"
								} else {
									rc = bat returnStatus: true, script: "sfdx force:org:delete --targetusername ${SCRATCH_ORG_SFDC_USERNAME} --noprompt"
								}

								if (rc != 0) {
									error 'Unable to delete scratch org'
								}
							}
						}
					}

				} else if(PR_BUILD_CHECK_SANDBOX){
					// Run the QA checks against a traditional sandbox as this project is not set up as a Salesforce DX
					// project
					unstash 'source'
					if(isPRForDevValidate()){//for PR to develop validate to BuildCheck
						withCredentials([file(credentialsId: PR_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
							deployMetadata(
									toolbelt,
									PR_CONNECTED_APP_CONSUMER_KEY,
									PR_USERNAME,
									orgSpecificJwtCredId,
									PR_HOST,
									CHANGED_FILES,
									PR_ORGNAME,
									true
							)
						}	
					}else if(isPRForQAValidate()){//for PR to qa validate to qa
						withCredentials([file(credentialsId: QACHECK_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
							deployMetadata(
									toolbelt,
									QACHECK_CONNECTED_APP_CONSUMER_KEY,
									QACHECK_USERNAME,
									orgSpecificJwtCredId,
									QACHECK_HOST,
									CHANGED_FILES,
									QACHECK_ORGNAME,
									true
							)
						}
					}else if(isPRForUatValidate()){//for PR to uat validate to uat
						withCredentials([file(credentialsId: UATCHECK_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
							deployMetadata(
									toolbelt,
									UATCHECK_CONNECTED_APP_CONSUMER_KEY,
									UATCHECK_USERNAME,
									orgSpecificJwtCredId,
									UATCHECK_HOST,
									CHANGED_FILES,
									UATCHECK_ORGNAME,
									true
							)
						}
					} else { //for PR to master validate to master
						withCredentials([file(credentialsId: VAL_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
							deployMetadata(
									toolbelt,
									VAL_CONNECTED_APP_CONSUMER_KEY,
									VAL_USERNAME,
									orgSpecificJwtCredId,
									VAL_HOST,
									CHANGED_FILES,
									VAL_ORGNAME,
									true
								)
							}
					}
				}
			}
		}


		// Last job for non-deployable pipeline
		if(!isDevDeployable() && !isQADeployable() && !isUatDeployable() && !isHotfix()){
			if(PR_ENABLE_NOTIFIER){
				try {
					bitbucketStatusNotify(
							buildState: 'SUCCESSFUL',
							buildKey: 'build',
							buildName: 'Build'
					)
				} catch(n){
					echo n.message
				}
			}
		}


		// Convert Salesforce DX source to traditional metadata
		//if(isDevDeployable() || isQADeployable() || isUatDeployable() || isHotfix()) {
			//stage('Convert') {
				//unstash 'source'

				// Convert the source code from DX to traditional, if this is a DX project
				//if(DX_FORMAT_PROJECT){
					//if(isUnix()){
						//rc = sh returnStatus: true, script: "${toolbelt}/sfdx force:source:convert -d mdapi/target/src"
					//} else {
						//rc = bat returnStatus: true, script: "sfdx force:source:convert -d mdapi/target/src"
					//}
				//}

				// Stash the build source code
				//stash name: 'source', includes: 'mdapi/'
			//}
		//}

		// Run MDoc
		//if(isDevDeployable()) {
			//stage('Documentation') {
				//unstash 'source'
				//def mkDirCommand = "mkdir documentation"
				//def mdocCommand = "java -jar build/lib/mdoc/mdoc.jar -X -C classes,objects,staticresources,triggers,pages,components,labels,workflow -S mdapi/src -T ./documentation -M 100 -A global,public,private,protected,webservice"

				//if(isUnix()){
					// Create a new documentation folder this build
					//rc = sh returnStatus: true, script: mkDirCommand

					// Run MDoc
					//rc = sh returnStatus: true, script: mdocCommand
				//} else {
					// Create a new documentation folder this build
					//rc = bat returnStatus: true, script: mkDirCommand

					// Run MDoc
					//rc = bat returnStatus: true, script: mdocCommand
				//}

				//publishHTML([
						//allowMissing: false,
						//alwaysLinkToLastBuild: true,
						//keepAll: true,
						//reportDir: 'documentation',
						//reportFiles: 'index.html',
						//reportName: 'MDoc Report'
				//])
			//}
		//}


		// Deploy the metadata to TEST
		if(isDevDeployable()) {
			lock(resource: TEST_USERNAME, inversePrecedence: true) {
				stage('TEST') {
					milestone()
					unstash 'source'

					withCredentials([file(credentialsId: TEST_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
						deployMetadata(
								toolbelt,
								TEST_CONNECTED_APP_CONSUMER_KEY,
								TEST_USERNAME,
								orgSpecificJwtCredId,
								TEST_HOST,
								CHANGED_FILES,
								TEST_ORGNAME,
								false
						)

						pushTag("${TEST_ORGNAME}/${NUMBER_NEXT_TAG}", GIT_CRED_ID, SCM_URL)
					}

					// Notify Tracker
					if(TRACKER_CHANGELOG_NOTIFIER_ENABLED){
						withCredentials([file(credentialsId: TRACKER_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
							updateTrackerSCMChangelog(
									toolbelt,
									TRACKER_CONNECTED_APP_CONSUMER_KEY,
									TRACKER_USERNAME,
									orgSpecificJwtCredId,
									TRACKER_HOST,
									CHANGE_LOG,
									TEST_ORGNAME
							)
						}
					}
				}

				sendStatusEmail('IN TEST');
			}
		}


		// Ask for approval before moving into UAT
		/*if(isDevDeployable()) {
			stage('Approval') {
				milestone()

				timeout(time: 1, unit: 'DAYS') {
					input 'Approved?'
				}

				milestone()
			}
		}*/

		// Deploy the metadata to QA
		if(isQADeployable()) {
			lock(resource: QA_USERNAME, inversePrecedence: true) {
				stage('QA') {
					milestone()
					unstash 'source'

					withCredentials([file(credentialsId: QA_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
						retry task: {
							deployMetadata(
									toolbelt,
									QA_CONNECTED_APP_CONSUMER_KEY,
									QA_USERNAME,
									orgSpecificJwtCredId,
									QA_HOST,
									CHANGED_FILES,
									QA_ORGNAME,
									false
							)

							pushTag("${QA_ORGNAME}/${NUMBER_NEXT_TAG}", GIT_CRED_ID, SCM_URL)
						}

						// Notify Tracker
						if(TRACKER_CHANGELOG_NOTIFIER_ENABLED){
							withCredentials([file(credentialsId: TRACKER_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
								updateTrackerSCMChangelog(
										toolbelt,
										TRACKER_CONNECTED_APP_CONSUMER_KEY,
										TRACKER_USERNAME,
										orgSpecificJwtCredId,
										TRACKER_HOST,
										CHANGE_LOG,
										TRACKER_ORGNAME
								)
							}
						}
					}
				}

				sendStatusEmail('IN QA');
			}
		}

		// Deploy the metadata to UAT
		if(isUatDeployable()) {
			lock(resource: UAT_USERNAME, inversePrecedence: true) {
				stage('UAT') {
					milestone()
					unstash 'source'

					withCredentials([file(credentialsId: UAT_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
						retry task: {
							deployMetadata(
									toolbelt,
									UAT_CONNECTED_APP_CONSUMER_KEY,
									UAT_USERNAME,
									orgSpecificJwtCredId,
									UAT_HOST,
									CHANGED_FILES,
									UAT_ORGNAME,
									false
							)

							pushTag("${UAT_ORGNAME}/${NUMBER_NEXT_TAG}", GIT_CRED_ID, SCM_URL)
						}

						// Notify Tracker
						if(TRACKER_CHANGELOG_NOTIFIER_ENABLED){
							withCredentials([file(credentialsId: TRACKER_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
								updateTrackerSCMChangelog(
										toolbelt,
										TRACKER_CONNECTED_APP_CONSUMER_KEY,
										TRACKER_USERNAME,
										orgSpecificJwtCredId,
										TRACKER_HOST,
										CHANGE_LOG,
										UAT_ORGNAME
								)
							}
						}
					}
				}

				sendStatusEmail('IN UAT');
			}
		}

		// Deploy the metadata to BUILDCHK
		if(!DX_FORMAT_PROJECT && PR_BUILD_CHECK_SANDBOX) {
			if (isDevDeployable() || isHotfix()) {
				lock(resource: PR_USERNAME, inversePrecedence: true) {
					stage('BUILDCHK') {
						milestone()
						unstash 'source'

						withCredentials([file(credentialsId: PR_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
							retry task: {
								deployMetadata(
										toolbelt,
										PR_CONNECTED_APP_CONSUMER_KEY,
										PR_USERNAME,
										orgSpecificJwtCredId,
										PR_HOST,
										CHANGED_FILES,
										PR_ORGNAME,
										false
								)
							}
						}
					}
				}
			}
			if (isQADeployable() || isHotfix()) {
				lock(resource: QACHECK_USERNAME, inversePrecedence: true) {
					stage('QACHECK') {
						milestone()
						unstash 'source'

						withCredentials([file(credentialsId: QACHECK_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
							retry task: {
								deployMetadata(
										toolbelt,
										QACHECK_CONNECTED_APP_CONSUMER_KEY,
										QACHECK_USERNAME,
										orgSpecificJwtCredId,
										QACHECK_HOST,
										CHANGED_FILES,
										QACHECK_ORGNAME,
										false
								)
							}
						}
					}
				}
			}
			if (isUatDeployable() || isHotfix()) {
				lock(resource: UATCHECK_USERNAME, inversePrecedence: true) {
					stage('UATCHECK') {
						milestone()
						unstash 'source'

						withCredentials([file(credentialsId: UATCHECK_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
							retry task: {
								deployMetadata(
										toolbelt,
										UATCHECK_CONNECTED_APP_CONSUMER_KEY,
										UATCHECK_USERNAME,
										orgSpecificJwtCredId,
										UATCHECK_HOST,
										CHANGED_FILES,
										UATCHECK_ORGNAME,
										false
								)
							}
						}
					}
				}
			}
		}


		// Ask for approval before moving into PRODUCTION
/* 		if(isDeployable() || isHotfix()) {
			stage('Approval') {
				milestone()

				timeout(time: 7, unit: 'DAYS') {
					input '!! WARNING PRODUCTION !! Approved?'
				}

				milestone()
			}
		} */


		// Deploy the metadata to PRODUCTION
		if(isProdDeployable() || isHotfix()) {
			lock(resource: PRODUCTION_USERNAME, inversePrecedence: true) {
				stage('PRODUCTION') {
					milestone()
					unstash 'source'

					withCredentials([file(credentialsId: PRODUCTION_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
						retry task: {
							deployMetadata(
									toolbelt,
									PRODUCTION_CONNECTED_APP_CONSUMER_KEY,
									PRODUCTION_USERNAME,
									orgSpecificJwtCredId,
									PRODUCTION_HOST,
									CHANGED_FILES,
									PRODUCTION_ORGNAME,
									false
							)

							// Notify Tracker
							if(TRACKER_CHANGELOG_NOTIFIER_ENABLED){
								withCredentials([file(credentialsId: TRACKER_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
									updateTrackerSCMChangelog(
											toolbelt,
											TRACKER_CONNECTED_APP_CONSUMER_KEY,
											TRACKER_USERNAME,
											orgSpecificJwtCredId,
											TRACKER_HOST,
											CHANGE_LOG,
											PRODUCTION_ORGNAME
									)
								}
							}

							pushTag("${PRODUCTION_ORGNAME}/${NUMBER_NEXT_TAG}", GIT_CRED_ID, SCM_URL)
						}
					}
				}

				sendStatusEmail('IN PRODUCTION');
			}
		}


		// Deploy the metadata to VALIDATE
		if(!DX_FORMAT_PROJECT && PR_BUILD_CHECK_SANDBOX) {
			if (isProdDeployable() || isHotfix()) {
				lock(resource: PR_USERNAME, inversePrecedence: true) {
					stage('VALIDATE') {
						milestone()
						unstash 'source'

						withCredentials([file(credentialsId: VAL_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
							retry task: {
								deployMetadata(
										toolbelt,
										VAL_CONNECTED_APP_CONSUMER_KEY,
										VAL_USERNAME,
										orgSpecificJwtCredId,
										VAL_HOST,
										CHANGED_FILES,
										VAL_ORGNAME,
										false
								)
							}
						}
					}
				}
			}
		}


		// Notify BitBucket that the build was successful
		if(!isProdDeployable() && !isHotfix()) {
			if(PR_ENABLE_NOTIFIER){
				try {
					bitbucketStatusNotify(
							buildState: 'SUCCESSFUL',
							buildKey: 'build',
							buildName: 'Build'
					)
				} catch(n){
					echo n.message
				}
			}
		}

		// Notify the developer recipients
		sendStatusEmail('SUCCESSFUL');
	}
	} catch(e) {
		// Notify BitBucket that the build failed
		if(!isProdDeployable()) {
			if(PR_ENABLE_NOTIFIER){
				try {
					bitbucketStatusNotify(
							buildState: 'FAILED',
							buildKey: 'build',
							buildName: 'Build',
							buildDescription: e.message
					)
				} catch(n){
					echo n.message
				}
			}
		}

		// Notify the developer recipients
		sendStatusEmail('FAILED');

		// Mark the build as failed
		throw e
	}

}


/***********************************************************************************************************************
 * Utility methods
 **********************************************************************************************************************/


/**
 * Here be dragons...!!!
 * JSON objects serialized by JSON Slurper are NOT serializable! This means you must wrap the call to the parseText
 * method within a @NonCPS (native code) function. Not doing this will lead to a cryptic exception during execution
 * indicating a serialization exception.
 */
@NonCPS
def jsonParse(def json) {
	new groovy.json.JsonSlurperClassic().parseText(json)
}


/**
 * Updates the latest commit with a new tag. If the tag already exists on the given commit then it will fail silently.
 * Note: ensure that the GIT username is the form of an email address or it will break this function.
 *
 * @param tag 				Tag to be pushed out to the repository on the commit which is currently being built.
 * @param credentialsId 	The ID of the Jenkins credential which holds the GIT username and password.
 * @param scmUrl  			The GIT repository URL which the tag will be pushed to.
 */
def pushTag(tag, credentialsId, scmUrl){
	echo "tag: ${tag}"
	echo "credentialsId: ${credentialsId}"
	echo "scmUrl: ${scmUrl}"

	withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: credentialsId, usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']]) {
		if(isUnix()){
			sh script: "git config --global user.name \"Jenkins\""
			sh script: "git config --global user.email \"${GIT_USERNAME}\""

			scmUrl = scmUrl.drop('https://'.length())

			try {
				sh returnStdout: true, script: "git tag -a ${tag} -m 'Jenkins'"
				sh returnStdout: true, script: "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${scmUrl} --tags"
			} catch(e){
				echo 'Unable to add tag to commit: ' + e.message
			}
		} else {
			bat script: "git config --global user.name \"Jenkins\""
			bat script: "git config --global user.email \"${GIT_USERNAME}\""

			scmUrl = scmUrl.split('\n')[1]
			echo = "scmUrl.split: ${scmUrl}"
			scmUrl = scmUrl.drop('https://'.length())
			echo = "scmUrl.drop: ${scmUrl}"

			try {
				bat returnStdout: true, script: "git tag -a ${tag} -m 'Jenkins'"
				bat returnStdout: true, script: "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${scmUrl} --tags"
			} catch(e){
				echo 'Unable to add tag to commit: ' + e.message
			}
		}
	}
}


/**
 * Executes given params.task closure and if it fails, asks the user if it should be retried.
 * The task will then be executed again. If the user clicks abort, an exception will be
 * thrown aborting the pipeline.
 *
 * After the task either completes successfully or after the user clicks abort,
 * the params.andFinally closure will be executed.
 *
 * Usage:
 * <pre><code>
 * retry task: {
 *     sh 'something-flaky-generating-html-reports'
 * }, andFinally: {
 *     publishHTML target: [reportDir: 'report', reportFiles: 'index.html', reportName: 'Report']
 * }
 * </pre></code>
 *
 * @params params [task: {}, andFinally: {}]
 */
def retry (params) {
	// waitUntil will retry until the given closure returns true
	waitUntil {
		try {
			// execute the task, if this fails, an exception will be thrown
			// and the params.andFinally() wont be called
			(params.task)()

			// Not all retry calls will have a finally statement, check if one has been provided
			if(params.andFinally) {
				(params.andFinally)()
			}

			// make waitUntil stop retrying this closure
			return true
		} catch(e) {
			try {
				// input asks the user for "Retry? Proceed or Abort". If the
				// user clicks Proceed, input will just return normally.
				// If the user clicks Abort, an exception will be thrown
				input "Retry?"
			} catch (userClickedAbort) {
				// user clicked abort, call the andFinally closure and re-throw the exception
				// to actually abort the pipeline

				// Not all retry calls will have a finally statement, check if one has been provided
				if(params.andFinally) {
					(params.andFinally)()
				}

				// User aborted, throw the exception
				throw userClickedAbort
			}

			// make waitUntil execute this closure again
			return false
		}
	}
}


/**
 * Sends a generic email notification to give an update on the status of the build.
 *
 * @param buildStatus Status of the build which will be included in the email message.
 */
def sendStatusEmail(String buildStatus) {
	// Default values
	def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
	def summary = "${subject} (${env.BUILD_URL})"
	def details = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'\n\nCheck console output at: ${env.BUILD_URL} [${env.BUILD_NUMBER}]"

	try {
		emailext (
				subject: subject,
				body: details,
				attachLog: true,
				recipientProviders: [[$class: 'DevelopersRecipientProvider']],
				to: "ESMADDxDSupportReleaseManagement@deloitte.es"
		)
	} catch(e){
		echo 'Unable to send email notification: ' + e.message
	}
}


/**
 * Utility method for determining if the current branch being built can be deployable to develop/Test. This helps
 * control which aspects of the build must be ran.
 */
def isDevDeployable() {
	echo 'Branch name: ' + env.BRANCH_NAME
	return env.BRANCH_NAME == 'develop'
}

/**
 * Utility method for determining if the current branch being built can be deployable to QA. This helps
 * control which aspects of the build must be ran.
 */
def isQADeployable() {
	echo 'Branch name: ' + env.BRANCH_NAME
	return env.BRANCH_NAME == 'qa'
}

/**
 * Utility method for determining if the current branch being built can be deployable to UAT. This helps
 * control which aspects of the build must be ran.
 */
def isUatDeployable() {
	echo 'Branch name: ' + env.BRANCH_NAME
	return env.BRANCH_NAME == 'uat'
}

/**
 * Utility method for determining if the current build request is a pull Request to qa. This helps
 * control which aspects of the build must be ran.
 */
def isPRForQAValidate() {
	echo 'Branch name: ' + env.BRANCH_NAME
	echo 'CHANGE_TARGET: ' + env.CHANGE_TARGET
	
	return env.BRANCH_NAME.startsWith('PR-')  && env.CHANGE_TARGET == 'qa'
}

/**
 * Utility method for determining if the current build request is a pull Request to uat. This helps
 * control which aspects of the build must be ran.
 */
def isPRForUatValidate() {
	echo 'Branch name: ' + env.BRANCH_NAME
	echo 'CHANGE_TARGET: ' + env.CHANGE_TARGET
	
	return env.BRANCH_NAME.startsWith('PR-')  && env.CHANGE_TARGET == 'uat'
}

/**
 * Utility method for determining if the current build request is a pull Request to develop. This helps
 * control which aspects of the build must be ran.
 */
def isPRForDevValidate() {
	echo 'Branch name: ' + env.BRANCH_NAME
	echo 'CHANGE_TARGET: ' + env.CHANGE_TARGET
	
	return env.BRANCH_NAME.startsWith('PR-')  && env.CHANGE_TARGET == 'develop'
}

/**
 * Utility method for determining if the current branch being built can be deployable to Master/Production. This helps
 * control which aspects of the build must be ran.
 */
def isProdDeployable() {
	echo 'Branch name: ' + env.BRANCH_NAME
	return env.BRANCH_NAME == 'master'
}


/**
 * Utility method for determining if the current branch being built can be deployable to Production, but is a hotifx
 * and must instead take an alternative route to Production.
 */
def isHotfix() {
	echo 'Branch name: ' + env.BRANCH_NAME
	return env.BRANCH_NAME.length() > 7 && env.BRANCH_NAME.substring(0, 7) == 'hotfix/'
}


/**
 * Finds all of the past builds which have not been successful before the current build. This method will
 * recursively call itself and will move to the previous build until it finds one which successful or until
 * it reaches no other build.
 *
 * @param passedBuilds 	A list passed by reference which will be populated with the build numbers which
 * 						have not been successful before the current build.
 * @param build  		Current build being checked.
 */
def lastSuccessfulBuild(passedBuilds, build) {
	if ((build != null) && (build.result != 'SUCCESS')) {
		passedBuilds.add(build)
		lastSuccessfulBuild(passedBuilds, build.getPreviousBuild())
	}
}


/**
 * Using the build references passed into this method, it will build a list of all of the changes found in
 * all of them and return a string.
 */
@NonCPS
def getChangeLog(passedBuilds) {
	def log = ""

	for (int x = 0; x < passedBuilds.size(); x++) {
		def currentBuild = passedBuilds[x];
		def changeLogSets = currentBuild.rawBuild.changeSets

		for (int i = 0; i < changeLogSets.size(); i++) {
			def entries = changeLogSets[i].items
			for (int j = 0; j < entries.length; j++) {
				def entry = entries[j]
				log += "${entry.msg} by ${entry.author}\n"

				def files = new ArrayList(entry.affectedFiles)
				for (int k = 0; k < files.size(); k++) {
					def file = files[k]
				}
			}
		}
	}

	return log
}


/**
 * Using the build references passed into this method, it will build a list of all of the changes found in
 * all of them and return a list containing the filenames which were modified.
 */
@NonCPS
def getChangedFiles(passedBuilds) {
	def changes = []

	for (int x = 0; x < passedBuilds.size(); x++) {
		def currentBuild = passedBuilds[x];
		def changeLogSets = currentBuild.rawBuild.changeSets

		for (int i = 0; i < changeLogSets.size(); i++) {
			def entries = changeLogSets[i].items
			for (int j = 0; j < entries.length; j++) {
				def entry = entries[j]

				def files = new ArrayList(entry.affectedFiles)
				for (int k = 0; k < files.size(); k++) {
					def file = files[k]
					changes.add("${file.path}")
				}
			}
		}
	}

	changes.unique { a, b -> a <=> b }
	return changes
}


/***********************************************************************************************************************
 * Stage specific methods
 **********************************************************************************************************************/

/**
 * Deploys traditional metadata files to a conventional sandbox.
 *
 * @param toolbelt 		Path to where the SFDX tooling is installed.
 * @param clientId 		The Connected App client ID which will be used for logging into that sandbox.
 * @param jwtkeyfile 	The path to the certificate which will be used to sign the JWT token when logging into the sandbox.
 * @param instanceurl  	The host name to use to log into Salesforce (e.g. https://login.salesforce.com)
 * @param changedFiles  List of changed files
 * @param orgName  		Name of the sandbox or Production environment
 * @param checkOnly     Set to true if the deployment should be check only deployment
 */
def deployMetadata(toolbelt, clientId, username, jwtkeyfile, instanceurl, changedFiles, orgName, checkOnly) {
	// Login into the 
	
	if(isUnix()){
		rc = sh returnStatus: true, script: "${toolbelt}/sfdx force:auth:jwt:grant --clientid ${clientId} --username ${username} --jwtkeyfile ${jwtkeyfile} --instanceurl ${instanceurl} --loglevel debug"
	} else {
		rc = bat returnStatus: true, script: "sfdx force:auth:jwt:grant --clientid ${clientId} --username ${username} --jwtkeyfile ${jwtkeyfile} --instanceurl ${instanceurl} --loglevel debug"
	}

	if (rc != 0) {
		error 'Failed to login into the org'
	}

	// Run the Flow Repo Handler
    // Flow repo handler is not needed in api 45 and later
	//runTheFlowRepoHandler(toolbelt, username);

	// Modify the profiles
	modifyProfiles()

	// Modify outbound messages
	//runOutboundMessageModifier(orgName)

	// Modify remote site security
	runRemoteSiteSecurityModifier(orgName)

	// Run anonymous Apex against this org
	if(!checkOnly) {
		executeAnonApex(toolbelt, username, changedFiles, 'pre')
	}

	// Deploy the converted code
	def checkOnlyArg = checkOnly? '--checkonly' : ''
	echo 'Deploy metadata '+ checkOnlyArg

    /*Workarround a problemas de creacion de Zip y solapamiento de entornos bajo la teoria de que no se puede ejecutar un empaquetado y despliegue
      de metadatos de forma concurrente en la maquina de Jenkins
    */
    lock(resource: 'SFDX', inversePrecedence: true) {
		if(isUnix()){
			//rmsg = sh returnStdout: true, returnStatus: false, script: "${toolbelt}/sfdx force:mdapi:deploy --targetusername ${username} --deploydir ./mdapi/target/src --testlevel RunLocalTests --json ${checkOnlyArg} --ignorewarnings"
			rmsg = sh returnStdout: true, returnStatus: false, script: "${toolbelt}/sfdx force:mdapi:deploy --targetusername ${username} --deploydir ./mdapi/target/src --testlevel NoTestRun --json ${checkOnlyArg} --ignorewarnings"
		} else {
			//rmsg = bat returnStdout: true, returnStatus: false, script: "sfdx force:mdapi:deploy --targetusername ${username} --deploydir ./mdapi/target/src --testlevel RunLocalTests --json ${checkOnlyArg} --ignorewarnings"
			//rmsg = bat returnStdout: true, returnStatus: false, script: "sfdx force:mdapi:deploy --targetusername ${username} --deploydir ./mdapi/target/src --testlevel NoTestRun --json ${checkOnlyArg} --ignorewarnings"
			//rmsg = rmsg.split('\n')[2].trim()
			echo "Command: @sfdx force:mdapi:deploy --targetusername ${username} --deploydir ./mdapi/target/src --testlevel NoTestRun --json ${checkOnlyArg} --ignorewarnings"
			rmsg = bat returnStdout: true, returnStatus: false, script: "@sfdx force:mdapi:deploy --targetusername ${username} --deploydir ./mdapi/target/src --testlevel NoTestRun --json ${checkOnlyArg} --ignorewarnings"
			echo 'Command result:' + rmsg
		}
	}

	def robj = jsonParse(rmsg)
	def deploymentId = robj.result.id
	echo 'deploymentId: '+ deploymentId
	
	// Update the user with the progress of the deployment
	def checkStatus;

	if(isUnix()){
		//checkStatus = sh returnStatus: true, script: "${toolbelt}/sfdx force:mdapi:deploy:report --targetusername ${username} -i ${deploymentId} --wait 30"
		//checkStatus = sh returnStatus: true, script: "${toolbelt}/sfdx force:mdapi:deploy --targetusername ${username} --deploydir mdapi/target/src --wait 30 --testlevel NoTestRun ${checkOnlyArg}"
        checkStatus = sh returnStatus: true, script: "${toolbelt}/sfdx force:mdapi:deploy:report --targetusername ${username} -i ${deploymentId} --wait 30"
	} else {
		//checkStatus = bat returnStatus: true, script: "sfdx force:mdapi:deploy:report --targetusername ${username} -i ${deploymentId} --wait 30"
		//checkStatus = bat returnStatus: true, script: "sfdx force:mdapi:deploy --targetusername ${username} --deploydir mdapi/target/src --wait 30 --testlevel NoTestRun ${checkOnlyArg}"
		echo "Command: @sfdx force:mdapi:deploy:report --targetusername ${username} -i ${deploymentId} --wait 30"
        checkStatus = bat returnStatus: true, script: "@sfdx force:mdapi:deploy:report --targetusername ${username} -i ${deploymentId} --wait 30"
	}

	if(checkStatus != 0){
		error 'Failed to retrieve status of deployment'
	}

	// Retrieve the accurate overall code coverage
	def orgDetails;

	if(isUnix()){
		orgDetails = sh returnStdout: true, returnStatus: false, script: "${toolbelt}/sfdx force:org:display --targetusername ${username} --json"
	} else {
		echo "Command @sfdx force:org:display --targetusername ${username} --json"
        orgDetails = bat returnStdout: true, returnStatus: false, script: "@sfdx force:org:display --targetusername ${username} --json"
		//orgDetails = orgDetails.split('\n')[2].trim()
        echo 'Command result:' + orgDetails;
	}

	def org = jsonParse("${orgDetails}")
	def accessToken = org.result.accessToken
	def instanceUrl = org.result.instanceUrl

	//withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
		if(isUnix()){
			sh "ant -f ./build/modules/codecoverage/build.xml -DdeploymentId=${deploymentId} -Dsf.sessionId=${accessToken} -Dsf.endpointUrl=${instanceUrl} -q"
		} else {
			bat "ant -f ./build/modules/codecoverage/build.xml -DdeploymentId=${deploymentId} -Dsf.sessionId=${accessToken} -Dsf.endpointUrl=${instanceUrl} -q"
		}
	//}


	// Publish the test results to Jenkins
	def coverageReport = "reports/TEST-Apex.xml";
	if (fileExists(coverageReport)) {
		junit keepLongStdio: true, allowEmptyResults: true, testResults: coverageReport
	}
}


/**
 * Runs the flow repo handler.
 *
 * @param  toolbelt 		Path to where the SFDX tooling is installed.
 * @param  username  		The target username to use to run the anonymous apex against a specified org.
 */
def runTheFlowRepoHandler(toolbelt, username){
	timeout(time: 10, unit: 'MINUTES') {
		//withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
			if(isUnix()){
				rc = sh returnStatus: true, script: "${toolbelt}/sfdx force:mdapi:retrieve -a 47.0 -r mdapi/target/src/flowDefTarget -k ./build/modules/flowrepohandler/package.xml -u ${username}"
				rc = sh returnStatus: true, script:  "ant -f ./build/modules/flowrepohandler/build.xml -Dscript.sfdx=${toolbelt} -Dsf.username=${username.trim()} -q"
			} else {
                rc = bat returnStatus: true, script: "sfdx force:mdapi:retrieve -a 47.0 -r mdapi/target/src/flowDefTarget -k ./build/modules/flowrepohandler/package.xml -u ${username}"
				bat "ant -f ./build/modules/flowrepohandler/build.xml -Dsf.username=${username.trim()} -q"
			}
		//}
	}
}


/**
 * Removes hard to handle aspects from profiles.
 */
def modifyProfiles(){
	def command = "ant -f ./build/modules/profilemod/build.xml"

	//withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
		if(isUnix()){
			sh command
		} else {
			bat command
		}
	//}
}


/**
 * Modifies the outbound message endpoints to be org-specific.
 */
def runOutboundMessageModifier(orgName){
	def command = "ant -f ./build/modules/outboundmessages/build.xml -Dsf.org=${orgName}"

	//withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
		if(isUnix()){
			sh command
		} else {
			bat command
		}
	//}
}


/**
 * Modifies the remote site security settings to be org-specific.
 */
def runRemoteSiteSecurityModifier(orgName){
	def command = "ant -f ./build/modules/remotesitesecurity/build.xml -Dsf.org=${orgName}"

	//withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
		if(isUnix()){
			sh command
		} else {
			bat command
		}
	//}
}


/**
 * Executes anonymous Apex against an org using the changed files for either pre/post deployment
 * steps.
 *
 * @param  toolbelt 		Path to where the SFDX tooling is installed.
 * @param  username  		The target username to use to run the anonymous apex against a specified org.
 * @param  changedFiles 	List of changed files to use to find the anonymous apex to execute
 * @param  prefix  			The file prefix to use to find the anonymous apex files. Valid values are
 *                    		"pre" or "post".
 */
def executeAnonApex(toolbelt, username, changedFiles, prefix){
	// Run anonymous Apex against this org
	echo 'Running anonymous Apex'

	if(changedFiles != null) {
		for (int i = 0; i < changedFiles.size(); i++) {
			def file = changedFiles[i]

			// Skip the file if the filename is obviously too short
			if (file.length() <= 12) continue;

			if(fileExists(file)) {
				if (file.substring(0, 8) == 'scripts/' && (file.substring(8, 11) == prefix || file.substring(8, 12) == prefix)) {
					echo "Executing: ${file}"

					if (isUnix()) {
						sh returnStdout: true, returnStatus: false, script: "${toolbelt}/sfdx force:apex:execute --targetusername ${username} --apexcodefile ${file} --json --loglevel debug"
					} else {
						bat returnStdout: true, returnStatus: false, script: "sfdx force:apex:execute --targetusername ${username} --apexcodefile ${file} --json --loglevel debug"
					}
				}
			}
		}
	}
}


/**
 * This will update Tracker with the complete change log of changes in the pipeline since the last Production 
 * deployment for the org which is being deployed to. This is useful to provide business with an insight into where
 * updates are currently deployed to.
 */
def updateTrackerSCMChangelog(toolbelt, clientId, username, jwtkeyfile, instanceurl, changelog, orgName) {
	// Login into the sandbox
	if(isUnix()){
		rc = sh returnStatus: true, script: "${toolbelt}/sfdx force:auth:jwt:grant --clientid ${clientId} --username ${username} --jwtkeyfile ${jwtkeyfile} --instanceurl ${instanceurl} --loglevel debug"
	} else {
		rc = bat returnStatus: true, script: "sfdx force:auth:jwt:grant --clientid ${clientId} --username ${username} --jwtkeyfile ${jwtkeyfile} --instanceurl ${instanceurl} --loglevel debug"
	}

	if (rc != 0) {
		error 'Failed to login into the org'
	}

	// Deploy the converted code
	changelog = StringEscapeUtils.escapeHtml(changelog)
	echo "Changelog: ${changelog}"
	def lines = changelog.split('\n');
	def log = '<ul>';

	// Fix for maximum command line length for windows
	int numLines = !isUnix() && lines.size() > 40? 40 : lines.size();

	for(int i=0; i < numLines; i++){
		log += "<li>${lines[i]}</li>"
	}

	if(numLines == 40){
		log += "<li>Change log too large.</li>"
	}

	log += '</ul>'
	log = log.replace('\n', '').replace('\r', '')

	if(isUnix()){
		rmsg = sh returnStdout: true, returnStatus: false, script: "${toolbelt}/sfdx force:data:record:create -s SCM_Change_Log__c -v \"Org__c=${orgName} Changelog__c='${log}'\" --targetusername ${username}"
	} else {
		rmsg = bat returnStdout: true, returnStatus: false, script: "sfdx force:data:record:create -s SCM_Change_Log__c -v \"Org__c=${orgName} Changelog__c='${log}'\" --targetusername ${username}"
	}

	echo 'Publishing changelog result: ' + rmsg
}